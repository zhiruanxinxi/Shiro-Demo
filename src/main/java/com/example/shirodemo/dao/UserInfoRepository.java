package com.example.shirodemo.dao;

import com.example.shirodemo.entity.UserInfo;
import org.springframework.data.repository.CrudRepository;

public interface UserInfoRepository extends CrudRepository<UserInfo, Long> {
    /**通过username查找用户信息;*/
    UserInfo findByname(String name);
}
