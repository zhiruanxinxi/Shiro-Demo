package com.example.shirodemo.service.impl;

import com.example.shirodemo.dao.UserInfoRepository;
import com.example.shirodemo.entity.UserInfo;
import com.example.shirodemo.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserInfoServiceImpl implements UserInfoService {

    @Autowired
    private UserInfoRepository userInfoRepository;

    @Override
    public UserInfo findByname(String name) {
        System.out.println("UserInfoServiceImpl.findByUsername()");
        return userInfoRepository.findByname(name);
    }

}
