package com.example.shirodemo.exception;

import com.alibaba.fastjson.support.spring.FastJsonJsonView;
import org.apache.shiro.authz.UnauthorizedException;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

public class UnauthorizedExceptionResolver implements HandlerExceptionResolver {

    @Override
    public ModelAndView resolveException(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) {
        System.out.println("===============异常开始==============");
        //如果是shiro无权操作，因为shiro 在操作auno等一部分不进行转发至无权限url
        if (e instanceof UnauthorizedException) {
            ModelAndView mv = new ModelAndView();
            /**
             * 返回视图
             */
            //ModelAndView mv = new ModelAndView("403");
            FastJsonJsonView view = new FastJsonJsonView();
            Map<String, String> attributes = new HashMap<>();
            attributes.put("code", "403");
            attributes.put("msg", e.getMessage());
            view.setAttributesMap(attributes);
            mv.setView(view);
            return mv;
        }
        e.printStackTrace();
        ModelAndView mv = new ModelAndView("error");
        mv.addObject("exception", e.toString().replaceAll("\n", "<br/>"));
        return mv;
    }

}
